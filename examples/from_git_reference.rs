use conventional_commits_next_version::Commits;
use semver::Version;

fn main() {
    let commits = Commits::from_git_reference("4.0.0", vec![])
        .expect("failed to find tag");

    let from_version = Version::new(4, 0, 0);
    let next_version = commits.get_next_version(from_version, true);
    println!("Next version: {}", next_version.to_string());
}
