use std::io::{stdin, Read};

use error_chain::bail;
use git2::{Oid, Repository, Revwalk};
use semver::Version;

use crate::error::*;
use crate::model::commits::commit::Commit;
use crate::model::monorepos::Monorepos;
use crate::utilities::version::*;

mod commit;

pub struct Commits {
    commits: Vec<Commit>,
}

fn get_commits_till_head_from_oid(
    repository: &Repository,
    from_commit_hash: Oid,
    monorepos: &Monorepos,
) -> Result<Commits> {
    fn get_commit_revwalker(repository: &Repository, from_commit_hash: Oid) -> Result<Revwalk> {
        let mut revwalk = repository.revwalk()?;

        revwalk.push_head()?;

        revwalk.hide(from_commit_hash)
            .chain_err(|| ErrorKind::UnknownCommitHashOnRevwalk(from_commit_hash))?;

        Ok(revwalk)
    }

    let commits_from_revwalk: Vec<_> = get_commit_revwalker(repository, from_commit_hash)?
        .map(|oid| Commit::from_git(repository, oid?, monorepos))
        .collect::<Result<_>>()?;

    let mut commits: Vec<_> = commits_from_revwalk
        .into_iter()
        .flatten()
        .collect();

    commits.reverse();
    Ok(Commits { commits })
}

impl Commits {
    pub fn from_stdin() -> Self {
        let mut message = String::new();
        stdin().read_to_string(&mut message).unwrap();

        Commits {
            commits: vec![Commit::from(message)],
        }
    }

    pub fn from_git_commit_hash(
        from_commit_hash: &str,
        monorepos: Vec<String>,
    ) -> Result<Self> {
        fn parse_to_oid(repository: &Repository, oid: &str) -> Result<Oid> {
            match oid.len() {
                0 => {
                    error!("Provided Git commit hash is empty and can not be parsed.");
                    bail!(ErrorKind::EmptyCommitHash);
                }
                1..=39 => {
                    trace!(
                        "Attempting to find a match for the short commit hash {:?}",
                        oid
                    );
                    let matching_oid_lowercase = oid.to_lowercase();

                    let mut revwalk = repository.revwalk()?;
                    revwalk.push_head()?;

                    let matched_commit_hashes: Vec<Oid> = revwalk
                        .into_iter()
                        .map(|result| {
                            return match result {
                                Ok(oid) => {
                                    let oid_lowercase = oid.to_string().to_lowercase();

                                    if oid_lowercase.starts_with(&matching_oid_lowercase) {
                                        return Some(oid);
                                    }

                                    None
                                }
                                Err(error) => {
                                    error!("{:?}", error);

                                    None
                                }
                            };
                        })
                        .flatten()
                        .collect();

                    match matched_commit_hashes.len() {
                        0 => {
                            bail!(ErrorKind::ShortCommitHashMatchesNoCommitHashes(matching_oid_lowercase));
                        }
                        1 => Ok(*matched_commit_hashes.first().unwrap()),
                        _ => {
                            bail!(ErrorKind::ShortCommitHashMatchesMultipleCommitHashes(matching_oid_lowercase, matched_commit_hashes))
                        }
                    }
                }
                40 => Ok(git2::Oid::from_str(oid)?),
                _ => {
                    bail!(ErrorKind::CommitHashTooLong(oid.to_string()))
                }
            }
        }

        let repository = Repository::open_from_env()?;
        let monorepos = Monorepos::from(monorepos);

        get_commits_till_head_from_oid(
            &repository,
            parse_to_oid(&repository, from_commit_hash)?,
            &monorepos,
        )
    }

    pub fn from_git_reference(
        reference: &str,
        monorepos: Vec<String>,
    ) -> Result<Self> {
        fn get_reference(repository: &Repository, matching: &str) -> Result<Oid> {
            let reference = repository.resolve_reference_from_short_name(matching)
                .chain_err(|| ErrorKind::UnknownReference(matching.to_string()))?;

            let commit = reference.peel_to_commit()?;

            trace!(
                "Matched {:?} to the reference {:?} at the commit hash '{}'.",
                matching,
                reference.name().unwrap(),
                commit.id()
            );

            Ok(commit.id())
        }

        let repository = Repository::open_from_env()?;
        let monorepos = Monorepos::from(monorepos);

        get_commits_till_head_from_oid(
            &repository,
            get_reference(&repository, reference)?,
            &monorepos,
        )
    }

    pub fn get_next_version(&self, mut from_version: Version, batch_commits: bool) -> Version {
        fn increment_version_batch(commits: &[Commit], version: &mut Version, pre_major: bool) {
            if commits
                .iter()
                .filter(|commit| commit.is_major_increment())
                .count()
                > 0
            {
                if pre_major {
                    increment_minor(version);
                } else {
                    increment_major(version);
                }
            } else if commits
                .iter()
                .filter(|commit| commit.is_minor_increment())
                .count()
                > 0
            {
                increment_minor(version);
            } else if commits
                .iter()
                .filter(|commit| commit.is_patch_increment())
                .count()
                > 0
            {
                increment_patch(version);
            }
        }

        fn increment_version_consecutive(
            commits: &[Commit],
            version: &mut Version,
            pre_major: bool,
        ) {
            commits.iter().for_each(|commit| {
                if commit.is_major_increment() {
                    if pre_major {
                        increment_minor(version);
                    } else {
                        increment_major(version);
                    }
                } else if commit.is_minor_increment() {
                    increment_minor(version);
                } else if commit.is_patch_increment() {
                    increment_patch(version);
                }
            });
        }

        let pre_major = from_version.major.eq(&0);

        if batch_commits {
            info!("Operating in batch mode.");
            increment_version_batch(&self.commits, &mut from_version, pre_major);
        } else {
            info!("Operating in consecutive mode.");
            increment_version_consecutive(&self.commits, &mut from_version, pre_major);
        }

        from_version
    }
}
