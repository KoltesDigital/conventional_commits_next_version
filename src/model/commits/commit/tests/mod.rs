use rstest::rstest;
use semver::Version;

use crate::model::commits::commit::Commit;
use crate::model::commits::Commits;

mod commit;
mod commits;
