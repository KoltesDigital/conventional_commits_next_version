#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;

pub use crate::model::commits::Commits;
pub use crate::utilities::version;

pub mod error;
mod model;
mod utilities;
