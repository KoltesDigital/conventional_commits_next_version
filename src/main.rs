#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate pretty_env_logger;
extern crate regex;

use error_chain::bail;
use structopt::StructOpt;

use crate::error::*;
use crate::model::commits::Commits;

mod cli;
mod error;
mod model;
mod utilities;

const ERROR_EXIT_CODE: i32 = 1;

fn run() -> Result<()> {
    pretty_env_logger::init();
    trace!("Version {}.", env!("CARGO_PKG_VERSION"));
    let arguments = cli::Arguments::from_args();
    trace!("The command line arguments provided are {:?}.", arguments);

    let commits = if arguments.from_stdin {
        Commits::from_stdin()
    } else if let Some(commit_hash) = arguments.from_commit_hash {
        Commits::from_git_commit_hash(&commit_hash, arguments.monorepo)?
    } else if let Some(reference) = arguments.from_reference {
        Commits::from_git_reference(&reference, arguments.monorepo)?
    } else {
        bail!("Provide either the --from-reference or --from-commit-hash argument.");
    };

    let expected_version =
        commits.get_next_version(arguments.from_version, arguments.batch_commits);

    if let Some(current_version) = arguments.current_version {
        if current_version < expected_version {
            bail!(
                "The current version {} is not larger or equal to the expected version {}.",
                current_version, expected_version
            );
        }
        info!(
            "The current version {} is larger or equal to the expected version {}.",
            current_version, expected_version
        );
    } else {
        print!("{}", expected_version.to_string());
    }

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        error!("{}", e);
        std::process::exit(ERROR_EXIT_CODE);
    }
}
