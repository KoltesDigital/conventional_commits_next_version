use error_chain::error_chain;
use git2::Oid;

error_chain! {
    foreign_links  {
        GenericGit(git2::Error);
    }

    errors {
        EmptyCommitHash {
            description("empty commit hash")
            display("Empty commit hash.")
        }

        CommitHashTooLong(commit_hash: String) {
            description("commit hash too long")
            display("Provided Git commit hash is too long and can not be parsed: '{}'.", commit_hash)
        }

        ShortCommitHashMatchesMultipleCommitHashes(commit_hash: String, matched_commit_hashes: Vec<Oid>) {
            description("short commit hash matches multiple commit hashes")
            display("Short commit hash '{}' matches multiple commit hashes: {:?}.", commit_hash, matched_commit_hashes)
        }

        ShortCommitHashMatchesNoCommitHashes(commit_hash: String) {
            description("short commit hash matches no commit hashes")
            display("Short commit hash '{}' matches no commit hashes.", commit_hash)
        }

        UnknownCommitHash(commit_hash: Oid) {
            description("unknown commit hash")
            display("Unknown commit hash '{}'.", commit_hash)
        }

        UnknownCommitHashOnRevwalk(commit_hash: Oid) {
            description("unknown commit hash on the Git revision walker")
            display("Unknown commit hash '{}' on the Git revision walker.", commit_hash)
        }

        UnknownReference(reference: String) {
            description("unknown Git reference")
            display("Unknown reference '{}'.", reference)
        }
    }
}
